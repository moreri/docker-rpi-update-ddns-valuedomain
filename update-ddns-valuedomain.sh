#!/bin/bash

trap 'exit' SIGHUP SIGINT SIGQUIT SIGTERM

while true
do
    curl "https://dyn.value-domain.com/cgi-bin/dyn.fcg?d=${DOMAIN}&p=${PASSWORD}${HOSTNAME:+&h=}${HOSTNAME}"
    sleep ${INTERVAL:=604800}
done
