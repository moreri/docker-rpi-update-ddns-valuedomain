# docker-rpi-update-ddns-valuedomain

## usage
`docker run --name=update-ddns-value-domain -d -e DOMAIN=<YourDomain> -e PASSWORD=<YourPassword> -e HOSTNAME=<YourHostname> -e INTERVAL=<Interval(sec)> moreri/rpi-update-ddns-valuedomain`

default INTERVAL is 604800 (a week).

