FROM alpine:latest
MAINTAINER moreri <moreri@gmail.com>

RUN apk update \
    && apk upgrade \
    && apk add tzdata \
    && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime \
    && echo "Asia/Tokyo" > /etc/timezone \
    && apk del tzdata \
    && apk add bash curl \
    && rm -rf /var/cache/apk/*

ADD update-ddns-valuedomain.sh /usr/local/bin

CMD ["/usr/local/bin/update-ddns-valuedomain.sh"]
 

